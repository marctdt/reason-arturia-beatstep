--
-- Generic Multi Generic Controller on MIDI Channel Remote Codec
--   by: Koshdukai
--
-- V.1.1.0 / Map V.1.0.0

function remote_init(manufacturer, model)
	chn="0"
	    if model == "BeatstepPro Sequencer 1" then
		chn="1"
	elseif model == "BeatstepPro Sequencer 2" then
		chn="2"
	elseif model == "Generic Controller on MIDI Channel  4 /<" then
		chn="3"
	elseif model == "Generic Controller on MIDI Channel  5 /<" then
		chn="4"
	elseif model == "Generic Controller on MIDI Channel  6 /<" then
		chn="5"
	elseif model == "Generic Controller on MIDI Channel  7 /<" then
		chn="6"
	elseif model == "Generic Controller on MIDI Channel  8 /<" then
		chn="7"
	elseif model == "Generic Controller on MIDI Channel  9 /<" then
		chn="8"
	elseif model == "BeatstepPro Drum" then
		chn="9"
	elseif model == "Generic Controller on MIDI Channel 11 /<" then
		chn="A"
	elseif model == "Generic Controller on MIDI Channel 12 /<" then
		chn="B"
	elseif model == "Generic Controller on MIDI Channel 13 /<" then
		chn="C"
	elseif model == "Generic Controller on MIDI Channel 14 /<" then
		chn="D"
	elseif model == "Generic Controller on MIDI Channel 15 /<" then
		chn="E"
	elseif model == "Generic Controller on MIDI Channel 16 /<" then
		chn="F"
	end

	local items=
	{
		{name="Keyboard",          	input="keyboard"},
		{name="Pitch Bend Wheel",  	input="value",	               	min=0,	max=16383},
		{name="Channel Aftertouch",	input="value",	               	min=0,	max=127},
		{name="Modulation Wheel",  	input="value",	output="value",	min=0,	max=127},
		{name="Sustain Pedal",     	input="value",	output="value",	min=0,	max=127},
		{name="Expression Pedal",  	input="value",	output="value",	min=0,	max=127},
		{name="Breath",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 00",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 03",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 04",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 05",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 06",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 07",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 08",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 09",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 10",             	input="value",	output="value",	min=0,	max=127},
--		{name="CC 11",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 12",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 13",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 14",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 15",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 16",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 17",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 18",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 19",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 20",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 21",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 22",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 23",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 24",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 25",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 26",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 27",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 28",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 29",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 30",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 31",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 32",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 33",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 34",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 35",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 36",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 37",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 38",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 39",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 40",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 41",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 42",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 43",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 44",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 45",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 46",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 47",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 48",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 49",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 50",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 51",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 52",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 53",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 54",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 55",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 56",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 57",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 58",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 59",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 60",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 61",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 62",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 63",             	input="value",	output="value",	min=0,	max=127},
--		{name="CC 64",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 65",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 66",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 67",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 68",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 69",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 70",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 71",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 72",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 73",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 74",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 75",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 76",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 77",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 78",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 79",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 80",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 81",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 82",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 83",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 84",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 85",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 86",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 87",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 88",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 89",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 90",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 91",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 92",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 93",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 94",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 95",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 96",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 97",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 98",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 99",             	input="value",	output="value",	min=0,	max=127},
		{name="CC 100",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 101",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 102",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 103",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 104",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 105",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 106",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 107",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 108",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 109",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 110",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 111",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 112",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 113",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 114",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 115",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 116",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 117",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 118",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 119",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 120",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 121",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 122",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 123",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 124",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 125",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 126",            	input="value",	output="value",	min=0,	max=127},
		{name="CC 127",            	input="value",	output="value",	min=0,	max=127},
	}
	remote.define_items(items)

	local inputs=
	{
		{pattern="<100x>"..chn.." yy zz",	name="Keyboard"},
		{pattern="9"..chn.." xx 00",     	name="Keyboard",         value="0", note="x", velocity="64"},
		{pattern="e"..chn.." xx yy",     	name="Pitch Bend Wheel", value="y * 128 + x"},
		{pattern="d"..chn.." xx",        	name="Channel Aftertouch"},
		{pattern="b"..chn.." 00 xx",     	name="CC 00"},
		{pattern="b"..chn.." 01 xx",     	name="Modulation Wheel"},
		{pattern="b"..chn.." 02 xx",     	name="Breath"},
		{pattern="b"..chn.." 03 xx",     	name="CC 03"},
		{pattern="b"..chn.." 04 xx",     	name="CC 04"},
		{pattern="b"..chn.." 05 xx",     	name="CC 05"},
		{pattern="b"..chn.." 06 xx",     	name="CC 06"},
		{pattern="b"..chn.." 07 xx",     	name="CC 07"},
		{pattern="b"..chn.." 08 xx",     	name="CC 08"},
		{pattern="b"..chn.." 09 xx",     	name="CC 09"},
		{pattern="b"..chn.." 0A xx",     	name="CC 10"},
		{pattern="b"..chn.." 0B xx",     	name="Expression Pedal"},
		{pattern="b"..chn.." 0C xx",     	name="CC 12"},
		{pattern="b"..chn.." 0D xx",     	name="CC 13"},
		{pattern="b"..chn.." 0E xx",     	name="CC 14"},
		{pattern="b"..chn.." 0F xx",     	name="CC 15"},
		{pattern="b"..chn.." 10 xx",     	name="CC 16"},
		{pattern="b"..chn.." 11 xx",     	name="CC 17"},
		{pattern="b"..chn.." 12 xx",     	name="CC 18"},
		{pattern="b"..chn.." 13 xx",     	name="CC 19"},
		{pattern="b"..chn.." 14 xx",     	name="CC 20"},
		{pattern="b"..chn.." 15 xx",     	name="CC 21"},
		{pattern="b"..chn.." 16 xx",     	name="CC 22"},
		{pattern="b"..chn.." 17 xx",     	name="CC 23"},
		{pattern="b"..chn.." 18 xx",     	name="CC 24"},
		{pattern="b"..chn.." 19 xx",     	name="CC 25"},
		{pattern="b"..chn.." 1A xx",     	name="CC 26"},
		{pattern="b"..chn.." 1B xx",     	name="CC 27"},
		{pattern="b"..chn.." 1C xx",     	name="CC 28"},
		{pattern="b"..chn.." 1D xx",     	name="CC 29"},
		{pattern="b"..chn.." 1E xx",     	name="CC 30"},
		{pattern="b"..chn.." 1F xx",     	name="CC 31"},
		{pattern="b"..chn.." 20 xx",     	name="CC 32"},
		{pattern="b"..chn.." 21 xx",     	name="CC 33"},
		{pattern="b"..chn.." 22 xx",     	name="CC 34"},
		{pattern="b"..chn.." 23 xx",     	name="CC 35"},
		{pattern="b"..chn.." 24 xx",     	name="CC 36"},
		{pattern="b"..chn.." 25 xx",     	name="CC 37"},
		{pattern="b"..chn.." 26 xx",     	name="CC 38"},
		{pattern="b"..chn.." 27 xx",     	name="CC 39"},
		{pattern="b"..chn.." 28 xx",     	name="CC 40"},
		{pattern="b"..chn.." 29 xx",     	name="CC 41"},
		{pattern="b"..chn.." 2A xx",     	name="CC 42"},
		{pattern="b"..chn.." 2B xx",     	name="CC 43"},
		{pattern="b"..chn.." 2C xx",     	name="CC 44"},
		{pattern="b"..chn.." 2D xx",     	name="CC 45"},
		{pattern="b"..chn.." 2E xx",     	name="CC 46"},
		{pattern="b"..chn.." 2F xx",     	name="CC 47"},
		{pattern="b"..chn.." 30 xx",     	name="CC 48"},
		{pattern="b"..chn.." 31 xx",     	name="CC 49"},
		{pattern="b"..chn.." 32 xx",     	name="CC 50"},
		{pattern="b"..chn.." 33 xx",     	name="CC 51"},
		{pattern="b"..chn.." 34 xx",     	name="CC 52"},
		{pattern="b"..chn.." 35 xx",     	name="CC 53"},
		{pattern="b"..chn.." 36 xx",     	name="CC 54"},
		{pattern="b"..chn.." 37 xx",     	name="CC 55"},
		{pattern="b"..chn.." 38 xx",     	name="CC 56"},
		{pattern="b"..chn.." 39 xx",     	name="CC 57"},
		{pattern="b"..chn.." 3A xx",     	name="CC 58"},
		{pattern="b"..chn.." 3B xx",     	name="CC 59"},
		{pattern="b"..chn.." 3C xx",     	name="CC 60"},
		{pattern="b"..chn.." 3D xx",     	name="CC 61"},
		{pattern="b"..chn.." 3E xx",     	name="CC 62"},
		{pattern="b"..chn.." 3F xx",     	name="CC 63"},
		{pattern="b"..chn.." 40 xx",     	name="Sustain Pedal"},
		{pattern="b"..chn.." 41 xx",     	name="CC 65"},
		{pattern="b"..chn.." 42 xx",     	name="CC 66"},
		{pattern="b"..chn.." 43 xx",     	name="CC 67"},
		{pattern="b"..chn.." 44 xx",     	name="CC 68"},
		{pattern="b"..chn.." 45 xx",     	name="CC 69"},
		{pattern="b"..chn.." 46 xx",     	name="CC 70"},
		{pattern="b"..chn.." 47 xx",     	name="CC 71"},
		{pattern="b"..chn.." 48 xx",     	name="CC 72"},
		{pattern="b"..chn.." 49 xx",     	name="CC 73"},
		{pattern="b"..chn.." 4A xx",     	name="CC 74"},
		{pattern="b"..chn.." 4B xx",     	name="CC 75"},
		{pattern="b"..chn.." 4C xx",     	name="CC 76"},
		{pattern="b"..chn.." 4D xx",     	name="CC 77"},
		{pattern="b"..chn.." 4E xx",     	name="CC 78"},
		{pattern="b"..chn.." 4F xx",     	name="CC 79"},
		{pattern="b"..chn.." 50 xx",     	name="CC 80"},
		{pattern="b"..chn.." 51 xx",     	name="CC 81"},
		{pattern="b"..chn.." 52 xx",     	name="CC 82"},
		{pattern="b"..chn.." 53 xx",     	name="CC 83"},
		{pattern="b"..chn.." 54 xx",     	name="CC 84"},
		{pattern="b"..chn.." 55 xx",     	name="CC 85"},
		{pattern="b"..chn.." 56 xx",     	name="CC 86"},
		{pattern="b"..chn.." 57 xx",     	name="CC 87"},
		{pattern="b"..chn.." 58 xx",     	name="CC 88"},
		{pattern="b"..chn.." 59 xx",     	name="CC 89"},
		{pattern="b"..chn.." 5A xx",     	name="CC 90"},
		{pattern="b"..chn.." 5B xx",     	name="CC 91"},
		{pattern="b"..chn.." 5C xx",     	name="CC 92"},
		{pattern="b"..chn.." 5D xx",     	name="CC 93"},
		{pattern="b"..chn.." 5E xx",     	name="CC 94"},
		{pattern="b"..chn.." 5F xx",     	name="CC 95"},
		{pattern="b"..chn.." 60 xx",     	name="CC 96"},
		{pattern="b"..chn.." 61 xx",     	name="CC 97"},
		{pattern="b"..chn.." 62 xx",     	name="CC 98"},
		{pattern="b"..chn.." 63 xx",     	name="CC 99"},
		{pattern="b"..chn.." 64 xx",     	name="CC 100"},
		{pattern="b"..chn.." 65 xx",     	name="CC 101"},
		{pattern="b"..chn.." 66 xx",     	name="CC 102"},
		{pattern="b"..chn.." 67 xx",     	name="CC 103"},
		{pattern="b"..chn.." 68 xx",     	name="CC 104"},
		{pattern="b"..chn.." 69 xx",     	name="CC 105"},
		{pattern="b"..chn.." 6A xx",     	name="CC 106"},
		{pattern="b"..chn.." 6B xx",     	name="CC 107"},
		{pattern="b"..chn.." 6C xx",     	name="CC 108"},
		{pattern="b"..chn.." 6D xx",     	name="CC 109"},
		{pattern="b"..chn.." 6E xx",     	name="CC 110"},
		{pattern="b"..chn.." 6F xx",     	name="CC 111"},
		{pattern="b"..chn.." 70 xx",     	name="CC 112"},
		{pattern="b"..chn.." 71 xx",     	name="CC 113"},
		{pattern="b"..chn.." 72 xx",     	name="CC 114"},
		{pattern="b"..chn.." 73 xx",     	name="CC 115"},
		{pattern="b"..chn.." 74 xx",     	name="CC 116"},
		{pattern="b"..chn.." 75 xx",     	name="CC 117"},
		{pattern="b"..chn.." 76 xx",     	name="CC 118"},
		{pattern="b"..chn.." 77 xx",     	name="CC 119"},
		{pattern="b"..chn.." 78 xx",     	name="CC 120"},
		{pattern="b"..chn.." 79 xx",     	name="CC 121"},
		{pattern="b"..chn.." 7A xx",     	name="CC 122"},
		{pattern="b"..chn.." 7B xx",     	name="CC 123"},
		{pattern="b"..chn.." 7C xx",     	name="CC 124"},
		{pattern="b"..chn.." 7D xx",     	name="CC 125"},
		{pattern="b"..chn.." 7E xx",     	name="CC 126"},
		{pattern="b"..chn.." 7F xx",     	name="CC 127"},

	}
	remote.define_auto_inputs(inputs)

	local outputs={
		{name="CC 00"           ,	pattern="b"..chn.." 00 xx"},
		{name="Modulation Wheel",	pattern="b"..chn.." 01 xx"},
		{name="Breath"          ,	pattern="b"..chn.." 02 xx"},
		{name="CC 03"           ,	pattern="b"..chn.." 03 xx"},
		{name="CC 04"           ,	pattern="b"..chn.." 04 xx"},
		{name="CC 05"           ,	pattern="b"..chn.." 05 xx"},
		{name="CC 06"           ,	pattern="b"..chn.." 06 xx"},
		{name="CC 07"           ,	pattern="b"..chn.." 07 xx"},
		{name="CC 08"           ,	pattern="b"..chn.." 08 xx"},
		{name="CC 09"           ,	pattern="b"..chn.." 09 xx"},
		{name="CC 10"           ,	pattern="b"..chn.." 0A xx"},
		{name="Expression Pedal",	pattern="b"..chn.." 0B xx"},
		{name="CC 12"           ,	pattern="b"..chn.." 0C xx"},
		{name="CC 13"           ,	pattern="b"..chn.." 0D xx"},
		{name="CC 14"           ,	pattern="b"..chn.." 0E xx"},
		{name="CC 15"           ,	pattern="b"..chn.." 0F xx"},
		{name="CC 16"           ,	pattern="b"..chn.." 10 xx"},
		{name="CC 17"           ,	pattern="b"..chn.." 11 xx"},
		{name="CC 18"           ,	pattern="b"..chn.." 12 xx"},
		{name="CC 19"           ,	pattern="b"..chn.." 13 xx"},
		{name="CC 20"           ,	pattern="b"..chn.." 14 xx"},
		{name="CC 21"           ,	pattern="b"..chn.." 15 xx"},
		{name="CC 22"           ,	pattern="b"..chn.." 16 xx"},
		{name="CC 23"           ,	pattern="b"..chn.." 17 xx"},
		{name="CC 24"           ,	pattern="b"..chn.." 18 xx"},
		{name="CC 25"           ,	pattern="b"..chn.." 19 xx"},
		{name="CC 26"           ,	pattern="b"..chn.." 1A xx"},
		{name="CC 27"           ,	pattern="b"..chn.." 1B xx"},
		{name="CC 28"           ,	pattern="b"..chn.." 1C xx"},
		{name="CC 29"           ,	pattern="b"..chn.." 1D xx"},
		{name="CC 30"           ,	pattern="b"..chn.." 1E xx"},
		{name="CC 31"           ,	pattern="b"..chn.." 1F xx"},
		{name="CC 32"           ,	pattern="b"..chn.." 20 xx"},
		{name="CC 33"           ,	pattern="b"..chn.." 21 xx"},
		{name="CC 34"           ,	pattern="b"..chn.." 22 xx"},
		{name="CC 35"           ,	pattern="b"..chn.." 23 xx"},
		{name="CC 36"           ,	pattern="b"..chn.." 24 xx"},
		{name="CC 37"           ,	pattern="b"..chn.." 25 xx"},
		{name="CC 38"           ,	pattern="b"..chn.." 26 xx"},
		{name="CC 39"           ,	pattern="b"..chn.." 27 xx"},
		{name="CC 40"           ,	pattern="b"..chn.." 28 xx"},
		{name="CC 41"           ,	pattern="b"..chn.." 29 xx"},
		{name="CC 42"           ,	pattern="b"..chn.." 2A xx"},
		{name="CC 43"           ,	pattern="b"..chn.." 2B xx"},
		{name="CC 44"           ,	pattern="b"..chn.." 2C xx"},
		{name="CC 45"           ,	pattern="b"..chn.." 2D xx"},
		{name="CC 46"           ,	pattern="b"..chn.." 2E xx"},
		{name="CC 47"           ,	pattern="b"..chn.." 2F xx"},
		{name="CC 48"           ,	pattern="b"..chn.." 30 xx"},
		{name="CC 49"           ,	pattern="b"..chn.." 31 xx"},
		{name="CC 50"           ,	pattern="b"..chn.." 32 xx"},
		{name="CC 51"           ,	pattern="b"..chn.." 33 xx"},
		{name="CC 52"           ,	pattern="b"..chn.." 34 xx"},
		{name="CC 53"           ,	pattern="b"..chn.." 35 xx"},
		{name="CC 54"           ,	pattern="b"..chn.." 36 xx"},
		{name="CC 55"           ,	pattern="b"..chn.." 37 xx"},
		{name="CC 56"           ,	pattern="b"..chn.." 38 xx"},
		{name="CC 57"           ,	pattern="b"..chn.." 39 xx"},
		{name="CC 58"           ,	pattern="b"..chn.." 3A xx"},
		{name="CC 59"           ,	pattern="b"..chn.." 3B xx"},
		{name="CC 60"           ,	pattern="b"..chn.." 3C xx"},
		{name="CC 61"           ,	pattern="b"..chn.." 3D xx"},
		{name="CC 62"           ,	pattern="b"..chn.." 3E xx"},
		{name="CC 63"           ,	pattern="b"..chn.." 3F xx"},
		{name="Sustain Pedal"   ,	pattern="b"..chn.." 40 xx"},
		{name="CC 65"           ,	pattern="b"..chn.." 41 xx"},
		{name="CC 66"           ,	pattern="b"..chn.." 42 xx"},
		{name="CC 67"           ,	pattern="b"..chn.." 43 xx"},
		{name="CC 68"           ,	pattern="b"..chn.." 44 xx"},
		{name="CC 69"           ,	pattern="b"..chn.." 45 xx"},
		{name="CC 70"           ,	pattern="b"..chn.." 46 xx"},
		{name="CC 71"           ,	pattern="b"..chn.." 47 xx"},
		{name="CC 72"           ,	pattern="b"..chn.." 48 xx"},
		{name="CC 73"           ,	pattern="b"..chn.." 49 xx"},
		{name="CC 74"           ,	pattern="b"..chn.." 4A xx"},
		{name="CC 75"           ,	pattern="b"..chn.." 4B xx"},
		{name="CC 76"           ,	pattern="b"..chn.." 4C xx"},
		{name="CC 77"           ,	pattern="b"..chn.." 4D xx"},
		{name="CC 78"           ,	pattern="b"..chn.." 4E xx"},
		{name="CC 79"           ,	pattern="b"..chn.." 4F xx"},
		{name="CC 80"           ,	pattern="b"..chn.." 50 xx"},
		{name="CC 81"           ,	pattern="b"..chn.." 51 xx"},
		{name="CC 82"           ,	pattern="b"..chn.." 52 xx"},
		{name="CC 83"           ,	pattern="b"..chn.." 53 xx"},
		{name="CC 84"           ,	pattern="b"..chn.." 54 xx"},
		{name="CC 85"           ,	pattern="b"..chn.." 55 xx"},
		{name="CC 86"           ,	pattern="b"..chn.." 56 xx"},
		{name="CC 87"           ,	pattern="b"..chn.." 57 xx"},
		{name="CC 88"           ,	pattern="b"..chn.." 58 xx"},
		{name="CC 89"           ,	pattern="b"..chn.." 59 xx"},
		{name="CC 90"           ,	pattern="b"..chn.." 5A xx"},
		{name="CC 91"           ,	pattern="b"..chn.." 5B xx"},
		{name="CC 92"           ,	pattern="b"..chn.." 5C xx"},
		{name="CC 93"           ,	pattern="b"..chn.." 5D xx"},
		{name="CC 94"           ,	pattern="b"..chn.." 5E xx"},
		{name="CC 95"           ,	pattern="b"..chn.." 5F xx"},
		{name="CC 96"           ,	pattern="b"..chn.." 60 xx"},
		{name="CC 97"           ,	pattern="b"..chn.." 61 xx"},
		{name="CC 98"           ,	pattern="b"..chn.." 62 xx"},
		{name="CC 99"           ,	pattern="b"..chn.." 63 xx"},
		{name="CC 100"          ,	pattern="b"..chn.." 64 xx"},
		{name="CC 101"          ,	pattern="b"..chn.." 65 xx"},
		{name="CC 102"          ,	pattern="b"..chn.." 66 xx"},
		{name="CC 103"          ,	pattern="b"..chn.." 67 xx"},
		{name="CC 104"          ,	pattern="b"..chn.." 68 xx"},
		{name="CC 105"          ,	pattern="b"..chn.." 69 xx"},
		{name="CC 106"          ,	pattern="b"..chn.." 6A xx"},
		{name="CC 107"          ,	pattern="b"..chn.." 6B xx"},
		{name="CC 108"          ,	pattern="b"..chn.." 6C xx"},
		{name="CC 109"          ,	pattern="b"..chn.." 6D xx"},
		{name="CC 110"          ,	pattern="b"..chn.." 6E xx"},
		{name="CC 111"          ,	pattern="b"..chn.." 6F xx"},
		{name="CC 112"          ,	pattern="b"..chn.." 70 xx"},
		{name="CC 113"          ,	pattern="b"..chn.." 71 xx"},
		{name="CC 114"          ,	pattern="b"..chn.." 72 xx"},
		{name="CC 115"          ,	pattern="b"..chn.." 73 xx"},
		{name="CC 116"          ,	pattern="b"..chn.." 74 xx"},
		{name="CC 117"          ,	pattern="b"..chn.." 75 xx"},
		{name="CC 118"          ,	pattern="b"..chn.." 76 xx"},
		{name="CC 119"          ,	pattern="b"..chn.." 77 xx"},
		{name="CC 120"          ,	pattern="b"..chn.." 78 xx"},
		{name="CC 121"          ,	pattern="b"..chn.." 79 xx"},
		{name="CC 122"          ,	pattern="b"..chn.." 7A xx"},
		{name="CC 123"          ,	pattern="b"..chn.." 7B xx"},
		{name="CC 124"          ,	pattern="b"..chn.." 7C xx"},
		{name="CC 125"          ,	pattern="b"..chn.." 7D xx"},
		{name="CC 126"          ,	pattern="b"..chn.." 7E xx"},
		{name="CC 127"          ,	pattern="b"..chn.." 7F xx"},
	}
	remote.define_auto_outputs(outputs)
end

function remote_probe()

	local controlRequest="F0 7E 7F 06 01 F7"

	local controlResponse="F0 7E 00 06 02 00 20 6B 02 00 07 ?? ?? ?? ?? ?? F7"
return {
		request=controlRequest,
		response=controlResponse,
	}
end

function remote_prepare_for_use()
	local retEvents={
	}
	return retEvents
end

function remote_release_from_use()
	local retEvents={
	}
	return retEvents
end

-- 20150605 /< V.1.0.0
-- 20160124 /< V.1.1.0


-- ------------------------------------------------------------------------
--                                                                         
-- Codec by Koshdukai                                           
--                                                                         
-- http://KoshdukaiMusicReason.blogspot.com                                
--                                                                         
-- Permission is given by the author to freely use this Codec for personal 
-- use.                                                                    
--                                                                         
-- Any commercial use, distribution or inclusion of this Codec in any other
-- Codec packs, libraries or software products needs an express permission 
-- from the author. Please contact him at Koshdukai@gmail.com              
--                                                                         
-- If any of the algorithms used in this Codec are used in your Codecs,    
-- please give credit where due to the author or any future contributors.  
--                                                                         
-- COVERED CODEC IS PROVIDED UNDER THIS LICENSE ON AN "AS IS" BASIS,       
-- WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,              
-- INCLUDING, WITHOUT LIMITATION, WARRANTIES THAT THE COVERED CODEC        
-- IS FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE          
-- OR NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY AND                
-- PERFORMANCE OF THE COVERED CODEC IS WITH YOU. SHOULD ANY COVERED        
-- CODEC PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT THE INITIAL              
-- NECESSARY SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER OF           
-- WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. NO USE          
-- OF ANY COVERED CODEC IS AUTHORIZED HERE UNDER, EXCEPT UNDER             
-- THIS DISCLAIMER.                                                        
-- DEVELOPER OR ANY OTHER FUTURE CONTRIBUTOR) ASSUME THE COST OF ANY       
--                                                                         
-- Use at your own risk!                                                   
--                                                                         
-- ------------------------------------------------------------------------
