function remote_init()
	local items={
		{name="Keyboard", input="keyboard"},
		{name="Mod Wheel", input="value", min=0, max=127},
		{name="Pitch Bend", input="value", min=0, max=16384},
		{name="Damper Pedal", input="value", min=0, max=127},


		--	{name="click", input="button"},
		--{name="quant", input="button"},
		--{name="rew", input="button"},
		--{name="fwd", input="button"},
		--{name="stop", input="button"},
		--{name="play", input="button"},
		--{name="record", input="button"},
		--{name="loop", input="button"},

		{name="stop", input="button"},
		{name="stopctrl", input="button"},
		{name="play", input="button"},
		{name="playctrl", input="button"},
		{name="pausectrl", input="button"},
		{name="resumectrl", input="button"},

		{name="record", input="delta"},
		

		{name="pad1", input="button"},
		{name="pad2", input="button"},
		{name="pad3", input="value", min=0, max=127},
		{name="pad4",input="value", min=0, max=127},
		{name="pad5", input="button"},
		{name="pad6", input="button"},
		{name="pad7", input="button"},
		{name="pad8", input="button"},
		{name="pad9On", input="button"},
		{name="pad9Off", input="button"},
		{name="pad10", input="button"},
		{name="pad11", input="value", min=0, max=127},
		{name="pad12", input="value", min=0, max=127},
		{name="pad13", input="button"},
		{name="pad14", input="button"},
		{name="pad15", input="button"},
		{name="pad16", input="button"},


		--{name="volume", input="delta"},
		--{name="cut", input="delta"},
		--{name="reso", input="delta"},
		--{name="lforate", input="delta"},
		--{name="lfoamt", input="delta"},
		--{name="chorus", input="delta"},
		--{name="atk1", input="delta"},
		--{name="dec1", input="delta"},
		--
		--{name="preset", input="delta"},
		--{name="param1", input="delta"},
		--{name="param2", input="delta"},
		--{name="param3", input="delta"},
		--{name="param4", input="delta"},
		--{name="delay", input="delta"},
		--{name="sus1", input="delta"},
		--{name="rel1", input="delta"},		

		{name="knob1", input="delta"},		
		{name="knob2", input="delta"},		
		{name="knob3", input="delta"},		
		{name="knob4", input="delta"},		
		{name="knob5", input="delta"},		
		{name="knob6", input="delta"},		
		{name="knob7", input="delta"},		
		{name="knob8", input="delta"},		
		{name="knob9", input="delta"},		
		{name="knob10", input="delta"},		
		{name="knob11", input="delta"},		
		{name="knob12", input="delta"},		
		{name="knob13", input="delta"},		
		{name="knob14", input="delta"},		
		{name="knob15", input="delta"},		
		{name="knob16", input="delta"},


		{name="step1", input="button"},
		{name="step2", input="button"},
		{name="step3", input="button"},
		{name="step4", input="button"},
		{name="step5", input="button"},
		{name="step6", input="button"},
		{name="step7", input="button"},
		{name="step8", input="button"},
		{name="step9", input="button"},
		{name="step10", input="button"},
		{name="step11", input="button"},
		{name="step12", input="button"},
		{name="step13", input="button"},
		{name="step14", input="button"},
		{name="step15", input="button"},
		{name="step16", input="button"},


		{name="bws", input="button"},
		{name="bwf", input="button"},
		{name="ffs", input="button"},
		{name="fff", input="button"},

			}
	remote.define_items(items)

	local inputs={

		{pattern="b0 66 7f", name="pad1", value="1"},
		{pattern="b0 67 7f", name="pad2", value="1"},
		{pattern="b0 68 xx", name="pad3"},
		{pattern="b0 69 xx", name="pad4"},
		{pattern="b0 6A 7f", name="pad5", value="1"},
		{pattern="b0 6B 7f", name="pad6", value="1"},
		{pattern="b0 6C 7f", name="pad7", value="1"},
		{pattern="b0 6D 7f", name="pad8", value="1"},
		--{pattern="b0 6E 7f", name="pad9", value="1"},
		{pattern="b0 6E 7f", name="pad9On", value="1"},
		{pattern="b0 6E 00", name="pad9Off", value="1"},
		{pattern="b0 6F 7f", name="pad10", value="1"},
		{pattern="b0 70 xx", name="pad11"},
		{pattern="b0 71 xx", name="pad12"},
		{pattern="b0 73 7f", name="pad13", value="1"},
		{pattern="b0 74 7f", name="pad14", value="1"},
		{pattern="b0 75 7f", name="pad15", value="1"},
		{pattern="b0 76 7f", name="pad16", value="1"},
		
		{pattern="b0 09 19", name="bwf", value="1"},
		{pattern="b0 09 32", name="bws", value="1"},
		{pattern="b0 09 4B", name="ffs", value="1"},
		{pattern="b0 09 64", name="fff", value="1"},
		{pattern="b0 33 7f", name="stop", value="1"},
		--{pattern="b0 33 7f", name="stopctrl", value="1"},
		{pattern="b0 36 7f", name="play", value="1"},



		{pattern="F0 7F 7F 06 01 F7", name="resumectrl", value="1"},
		{pattern="F0 7F 7F 06 09 F7", name="pausectrl", value="1"},
		{pattern="F0 7F 7F 06 02 F7", name="playctrl", value="1"},
		{pattern="F0 7F 7F 06 xx F7", name="record",value="13 - (x * 2)"},

	


		{pattern="b0 0A xx", name="knob1", value="(64 - x) * -1"},
		--{pattern="b0 07 xx", name="knob1", value="(64 - x) * -1"},
		{pattern="b0 4A xx", name="knob2", value="(64 - x) * -1"},
		{pattern="b0 47 xx", name="knob3", value="(64 - x) * -1"},
		{pattern="b0 4C xx", name="knob4", value="(64 - x) * -1"},
		{pattern="b0 4D xx", name="knob5", value="(64 - x) * -1"},
		{pattern="b0 5D xx", name="knob6", value="(64 - x) * -1"},
		{pattern="b0 49 xx", name="knob7", value="(64 - x) * -1"},
		{pattern="b0 4B xx", name="knob8", value="(64 - x) * -1"},
								   
		{pattern="b0 72 xx", name="knob9", value="(64 - x) * -1"},
		{pattern="b0 12 xx", name="knob10", value="(64 - x) * -1"},
		{pattern="b0 13 xx", name="knob11", value="(64 - x) * -1"},
		{pattern="b0 10 xx", name="knob12", value="(64 - x) * -1"},
		{pattern="b0 11 xx", name="knob13", value="(64 - x) * -1"},
		{pattern="b0 5B xx", name="knob14", value="(64 - x) * -1"},
		{pattern="b0 4F xx", name="knob15", value="(64 - x) * -1"},
		{pattern="b0 48 xx", name="knob16", value="(64 - x) * -1"},

		{pattern="e0 xx yy", name="Pitch Bend", value="y*128 + x"},

		{pattern="b0 01 xx", name="Mod Wheel"},
		{pattern="b0 40 xx", name="Damper Pedal"},

		{pattern="80 xx yy", name="Keyboard", value="0", note="x", velocity="64"},		
		{pattern="90 xx 00", name="Keyboard", value="0", note="x", velocity="64"},
		{pattern="<100x>0 yy zz", name="Keyboard"},


		{pattern="b0 14 7f", name="step1", value="1"},
		{pattern="b0 15 7f", name="step2", value="1"},
		{pattern="b0 16 7f", name="step3", value="1"},
		{pattern="b0 17 7f", name="step4", value="1"},
		{pattern="b0 18 7f", name="step5", value="1"},
		{pattern="b0 19 7f", name="step6", value="1"},
		{pattern="b0 1A 7f", name="step7", value="1"},
		{pattern="b0 1B 7f", name="step8", value="1"},
		{pattern="b0 1C 7f", name="step9", value="1"},
		{pattern="b0 1D 7f", name="step10", value="1"},
		{pattern="b0 1E 7f", name="step11", value="1"},
		{pattern="b0 1F 7f", name="step12", value="1"},
		{pattern="b0 34 7f", name="step13", value="1"},
		{pattern="b0 35 7f", name="step14", value="1"},
		{pattern="b0 37 7f", name="step15", value="1"},
		{pattern="b0 38 7f", name="step16", value="1"},


	
	}
	remote.define_auto_inputs(inputs)
end


function remote_probe()

	local controlRequest="F0 7E 7F 06 01 F7"

	local controlResponse="F0 7E 00 06 02 00 20 6B 02 00 07 ?? ?? ?? ?? ?? F7"
return {
		request=controlRequest,
		response=controlResponse,
	}
end

