SYSEX_REQUEST = "F0 7E 7F 06 01 F7"
SYSEX_RESPONSE = "F0 7E 00 06 02 00 20 6B 02 00 06 00 ?? ?? ?? ?? F7"


ccNumbersPads = { "14" ,"15", "16", "17", "18", "19", "1A", "1B", "1C", "1D", "1E", "1F", "23", "29", "2E", "2F" }

instrumentCount = 10
padItemStartIndex = nil
playingStepIndex = nil
playingStepHasChanged = false
oldSelectInstrumentIndex = -1
newSelectInstrumentIndex = 0
selectedDrumIndex = nil
accentIndex = nil
stopButtonIndex = nil

local function toHex(number)
  return string.format("%02x", number)
end

function remote_init()

  local items = {}
  local inputs = {}
  local outputs = {}

  table.insert(items, {name="Keyboard", input="keyboard"})
		
  local itemName = "knob0"
  local ccNumber = "07"
  table.insert(items, { name = itemName, input = "delta" } )
  table.insert(inputs, { pattern = "B? ".. ccNumber .. " xx", name = itemName , value = "(64 - x) * -1"} )

  padItemStartIndex = table.getn(items)
  local padCount = 1
  for i, cc in ipairs(ccNumbersPads) do
    local itemName = "pad" .. padCount
    table.insert(items, { name = itemName, input = "value", output = "value", min = 0, max = 127 } )
    table.insert(inputs, { pattern = "B? ".. cc .. " xx", name = itemName } )
    table.insert(outputs, { pattern = "B? ".. cc .. " xx", name = itemName } )
    --    table.insert(outputs, { pattern = "B? " .. ccNumber .. " xx", name = itemName, x = "value" } )
    padCount = padCount + 1
  end

  for i = 1, 6 do
    local itemName = "pad" .. padCount
    table.insert(items, { name = itemName, input = "value", output = "value", min = 0, max = 127 } )
    padCount = padCount + 1
  end

  local ccNumbersEncoders = { "0A", "4A", "47", "4C", "4D", "5D", "49", "4B", "72", "12", "13", "10", "11", "5B", "4F", "48" }

  for i, ccNumber in ipairs(ccNumbersEncoders) do
    local itemName = "knob" .. i
    table.insert(items, { name = itemName, input = "delta" } )
    table.insert(inputs, { pattern = "B? ".. ccNumber .. " xx", name = itemName , value = "(64 - x) * -1"} )
--/    table.insert(outputs, { pattern = "B? " .. ccNumber .. " xx", name = itemName } )
  end

  local itemName = "play"
  table.insert(items, {name = itemName, input = "value", output = "value", min = 0, max = 127})
  table.insert(inputs, {pattern = "B? 34 xx" , name = itemName})
  table.insert(outputs, {pattern = "B? 34 xx" , name = itemName})

  local itemName = "stop"
  table.insert(items, {name = itemName, input = "value", output = "value", min = 0, max = 127})
  table.insert(inputs, {pattern = "B? 35 xx" , name = itemName})
  table.insert(outputs, {pattern = "B? 35 xx" , name = itemName})

table.insert(items,{name="Mod Wheel", input="value", min=0, max=127})
		table.insert(items,{name="Pitch Bend", input="value", min=0, max=16384})
		table.insert(items,{name="Damper Pedal", input="value", min=0, max=127})
		table.insert(inputs,{pattern="e0 xx yy", name="Pitch Bend", value="y*128 + x"})

		table.insert(inputs,{pattern="b0 01 xx", name="Mod Wheel"})
		table.insert(inputs,{pattern="b0 40 xx", name="Damper Pedal"})

		table.insert(inputs,{pattern="80 xx yy", name="Keyboard", value="0", note="x", velocity="64"})
		table.insert(inputs,{pattern="90 xx 00", name="Keyboard", value="0", note="x", velocity="64"})
		table.insert(inputs,{pattern="<100x>? yy zz", name="Keyboard"})

  remote.define_items(items)
  remote.define_auto_inputs(inputs)
  remote.define_auto_outputs(outputs)

end


function remote_probe(manufacturer, model, prober)
   local request_events={remote.make_midi(SYSEX_REQUEST)}
   local function match_events(mask,events)
 
     for i,event in ipairs(events) do
       local res=remote.match_midi(mask,event)
       if res~=nil then
         return true
       end
     end
     return false
   end
 
   local results={}
 
   for outPortIndex = 1,prober.out_ports do
     prober.midi_send_function(outPortIndex,request_events)
     prober.wait_function(50)
 
     local responding_ports={}
 
     for inPortIndex = 1,prober.in_ports do
       local events=prober.midi_receive_function(inPortIndex)
       if match_events(SYSEX_RESPONSE,events) then
         table.insert(responding_ports,inPortIndex)
       end
     end
 
     if responding_ports[1]~=nil then
       local ins={ responding_ports[1] }
 
       if responding_ports[2]~=nil then
         -- Second in port is optional
         table.insert(ins,responding_ports[2])
       end
       local one_result={ in_ports=ins, out_ports={outPortIndex}}
       table.insert(results,one_result)
     end
   end
   return results
 end
-- 
-- function trace(msg)
  -- remote.trace(tostring(msg) .. "\n")
-- -- -- -- -- -- end
